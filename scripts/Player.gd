extends Area2D
signal hit
signal enemy_destroyed
@export var speed = 400
var is_dashing_or_on_cooldown = false
var screen_size

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	$ShieldPivot.hide()
	$ShieldPivot/Shield/ShieldArea/CollisionPolygon2D.disabled = true
	hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2.ZERO
	if Input.is_action_pressed("move_right"):
		velocity.x += 1
	if Input.is_action_pressed("move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("move_down"):
		velocity.y += 1
		
	if Input.is_action_pressed("dash") && !is_dashing_or_on_cooldown:
		$ShieldPivot.show()
		$ShieldPivot/Shield/ShieldArea/CollisionPolygon2D.disabled = false
		speed = speed * 1.5
		is_dashing_or_on_cooldown = true
		$DashTimer.start()
		
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite2D.play()
	else:
		$AnimatedSprite2D.stop()
		
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	if velocity.x != 0:
		$AnimatedSprite2D.animation = "walk"
		$AnimatedSprite2D.flip_h = velocity.x < 0
	if velocity.y !=0:
		$AnimatedSprite2D.animation = "up"
		$AnimatedSprite2D.flip_v = velocity.y > 0
	
	velocity = velocity.normalized()
	$ShieldPivot.rotation = deg_to_rad(0 if velocity.y <= 0 else 180) + deg_to_rad(velocity.x * 90 * (-1 if velocity.y > 0 else 1))


func _on_body_entered(_body):
	hide()
	hit.emit()
	$CollisionShape2D.set_deferred("disabled", true)
	
func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false


func _on_shield_area_body_entered(body):
	body.queue_free()
	enemy_destroyed.emit()


func _on_dash_timer_timeout():
	speed = 400
	$ShieldPivot.hide()
	$ShieldPivot/Shield/ShieldArea/CollisionPolygon2D.disabled = true
	$DashCooldown.start()


func _on_dash_cooldown_timeout():
	is_dashing_or_on_cooldown = false
