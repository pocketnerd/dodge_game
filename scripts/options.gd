extends CanvasLayer
signal back_to_title
signal sound_value_changed(new_value)
signal music_value_changed(new_value)

# Called when the node enters the scene tree for the first time.
func _ready():
	self.hide()


func _on_back_button_pressed():
	back_to_title.emit()


func _on_sound_slider_value_changed(value):
	sound_value_changed.emit(value)


func _on_music_slider_value_changed(value):
	music_value_changed.emit(value)
