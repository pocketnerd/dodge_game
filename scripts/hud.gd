extends CanvasLayer
signal start_game
signal open_options

func _ready():
	$ScoreLabel.hide()

func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()
	
func show_game_over():
	show_message("Game Over")
	await $MessageTimer.timeout
	
	$Message.text = "Dodge the Creeps!"
	$Message.show()
	await get_tree().create_timer(1.0).timeout
	$StartButton.show()
	$OptionsButton.show()
	$ScoreLabel.hide()
	
func update_score(score):
	$ScoreLabel.text = str(score)


func _on_message_timer_timeout():
	$Message.hide()


func _on_start_button_pressed():
	$StartButton.hide()
	$OptionsButton.hide()
	$ScoreLabel.show()
	start_game.emit()


func _on_options_button_pressed():
	open_options.emit()
