extends Node
@export var mob_scene: PackedScene
var score


func game_over():
	$Music.stop()
	$DeathSound.play()
	$DeathEffect.position = $Player.position
	$DeathEffect.restart()
	$ScoreTimer.stop()
	$MobTimer.stop()
	await get_tree().create_timer(1.25).timeout
	$HUD.show_game_over()

func new_game():
	get_tree().call_group("mobs", "queue_free")
	score = 0
	$Music.play()
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Get Ready")
	

func _on_mob_timer_timeout():
	var mob = mob_scene.instantiate()
	# Choose a random location on Path2D.
	var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
	mob_spawn_location.progress_ratio = randf()

	# Set the mob's direction perpendicular to the path direction.
	var direction = mob_spawn_location.rotation + PI / 2

	# Set the mob's position to a random location.
	mob.position = mob_spawn_location.position

	# Add some randomness to the direction.
	direction += randf_range(-PI / 4, PI / 4)
	mob.rotation = direction

	# Choose the velocity for the mob.
	var velocity = Vector2(randf_range(150.0, 250.0), 0.0)
	mob.linear_velocity = velocity.rotated(direction)

	# Spawn the mob by adding it to the Main scene.
	add_child(mob)


func _on_score_timer_timeout():
	score += 1
	$HUD.update_score(score)


func _on_start_timer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()


func _on_player_enemy_destroyed():
	score += 1
	$HUD.update_score(score)


func _on_hud_open_options():
	$HUD.hide()
	$Options.show()


func _on_options_back_to_title():
	$HUD.show()
	$Options.hide()


func _on_options_music_value_changed(new_value):
	var value_in_db = linear_to_db(new_value)
	var bus_index = AudioServer.get_bus_index("Music")
	AudioServer.set_bus_volume_db(bus_index, value_in_db)
